<?php

/**
 * @file
 * NBePay menu items.
 */

function uc_nbepay_complete($order_id = 0) {
  $_debug = variable_get('uc_nbepay_debug', FALSE);

  $errmesg = t('We are sorry. An error occurred while processing your order that prevents us from completing it at this time. Please contact us and we will resolve the issue as soon as possible.');

  watchdog('NBePay', 'Receiving NBePay notification for order @order_id. <pre>@debug</pre>',
            array('@order_id' => $order_id, '@debug' => print_r($_POST, TRUE)));

  // Invalid transaction
  if ($_POST['orderid'] != $order_id) {
    watchdog('NBePay', 'Invalid NBePay transaction with mismatch ID, order #!order_id != #!orderid.', array('!order_id' => $order_id, '!orderid' => $_POST['orderid']), WATCHDOG_ERROR);
    drupal_set_message($errmesg, 'error');
    drupal_goto('cart');
  }

  // Assign posted variables to local variables
  $orderid  = check_plain($_POST['orderid']);
  $appcode  = check_plain($_POST['appcode']);
  $tran_id  = check_plain($_POST['tranID']);
  $domain   = check_plain($_POST['domain']);
  $status   = check_plain($_POST['status']);
  $amount   = check_plain($_POST['amount']);
  $currency = check_plain($_POST['currency']);
  $paydate  = check_plain($_POST['paydate']);
  $channel  = check_plain($_POST['channel']);
  $skey     = check_plain($_POST['skey']);
  $vkey     = variable_get('uc_nbepay_secret_word', '');

  $key0 = md5($tran_id . $orderid . $status . $domain . $amount . $currency);
  $key1 = md5($paydate . $domain . $key0 . $appcode . $vkey);

  $details = array(
    'orderid'   => $orderid,
    'appcode'   => $appcode,
    'tranID'    => $tran_id,
    'domain'    => $domain,
    'status'    => $status,
    'amount'    => $amount,
    'currency'  => $currency,
    'paydate'   => $paydate,
    'channel'   => $channel,
    'skey'      => $skey,
  );

  if ($_debug) watchdog('NBePay', 'key0 = @key0, key1 = @key1, skey = @skey. <pre>@details</pre>', array('@key0' => $key0, '@key1' => $key1, '@skey' => $skey, '@details' => print_r($details, TRUE)), WATCHDOG_DEBUG);

  // Invalid transaction
  if ($skey != $key1) {
    watchdog('NBePay', 'Invalid NBePay transaction attempted for order !order_id.', array('!order_id' => $orderid), WATCHDOG_ERROR);
    drupal_set_message($errmesg, 'error');
    drupal_goto('cart');
  }

  // Successful transaction
  if ($status == '00') {
    $order = uc_order_load($orderid);

    // Invalid order id
    if ($order === FALSE || uc_order_status_data($order->order_status, 'state') != 'in_checkout') {
      watchdog('NBePay', 'Invalid NBePay transaction attempted for non-existent order !order_id.', array('!order_id' => $orderid), WATCHDOG_ERROR);
      drupal_set_message($errmesg, 'error');
      drupal_goto('cart');
    }

    // TODO: payment status query (for CC only?)
    // Payment status query
  /*
  // Not done because NBePay not resending updated status notification
  // Until they do, there is no point in checking the status, unless we
  // make it into cron?

    $req = '';

    $qkey = md5($orderid . $domain . $vkey . $amount);
    $query = array(
      'amount'  => $amount,
      'oID'     => $orderid,
      'domain'  => $domain,
      'skey'    => $qkey,
    );
    foreach ($query as $key => $value) {
      $value = urlencode(stripslashes($value));
      $req .= $key .'='. $value .'&';
    }


    $host = 'https://www.onlinepayment.com.my/NBepay/query/query.php';

    $response = drupal_http_request($host, array(), 'POST', $req);

    if ($_debug) watchdog('NBePay', 'Transaction verification: <pre>@response</pre>', array('@response' => print_r($response, TRUE)), WATCHDOG_DEBUG);

    if (array_key_exists('error', $response)) {
      watchdog('NBePay', 'NBePay failed with HTTP error @error, code @code.', array('@error' => $response->error, '@code' => $response->code), WATCHDOG_ERROR);
      drupal_set_message($errmesg, 'error');
      drupal_goto('cart');
    }

    // TODO: more detailed verification, verify the response

    if (strcmp($response->data, 'settled') == 0) {
  */
      watchdog('NBePay', 'NBePay transaction verified for order !order_id.', array('!order_id' => $orderid));

      $comment = t('Paid by !channel via NBePay on !date (Transaction ID #!tran_id, Bank Approval #!appcode).',
                    array('!channel' => $channel, '!date' => $paydate, '!tran_id' => $tran_id, '!appcode' => $appcode));
      uc_payment_enter($order->order_id, 'NBePay', $amount, 0, $details, $comment);

      $context = array(
        'revision' => 'themed',
        'location' => 'nbepay-form',
      );
      $options = array(
        'sign' => variable_get('uc_currency_sign', '$'),
        'dec' => '.',
        'thou' => variable_get('uc_currency_thou', ','),
      );

      $output = uc_cart_complete_sale($order);
      uc_order_comment_save($order->order_id, 0, t('Payment of @amount submitted through @channel via NBePay.', array('@amount' => uc_price($amount, $context, $options), '@channel' => $channel)), 'order', 'payment_received');

      $page = variable_get('uc_cart_checkout_complete_page', '');
      if (!empty($page)) {
        drupal_goto(variable_get('uc_cart_checkout_complete_page', ''));
      }

      return $output;

  /*
  // payment status query
    }
    elseif (strcmp($response->data, 'failed') == 0) {
      watchdog('NBePay', 'NBePay transaction failed verification.', array(), WATCHDOG_ERROR);
      uc_order_comment_save($order_id, 0, t('An NBePay transaction failed verification for this order.'), 'admin');
      drupal_set_message($errmesg, 'error');
      drupal_goto('cart');
    }
  */
  }
  else {
    watchdog('NBePay', 'Unsuccessful NBePay transaction for order !order_id.', array('!order_id' => $orderid), WATCHDOG_ERROR);
    uc_order_comment_save($orderid, 0, t('A NBePay transaction was not successful for this order.'), 'admin');
    drupal_set_message($errmesg, 'error');
    drupal_goto('cart');
  }
}
