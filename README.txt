
-- SUMMARY --

This module provides Ubercart integration with the Malaysia payment gateway
NetBuilder's NBePay, through onlinepayment.com.my website.

For a full description visit the project page:
  http://drupal.org/project/uc_nbepay
Bug reports, feature suggestions and latest developments:
  http://drupal.org/project/issues/uc_nbepay


-- INSTALLATION --

Copy and extract the uc_nbepay module to your modules directory, usually
sites/all/modules and enable it on the Modules page (admin/build/modules).


-- CONFIGURATION --
* Configure the module at Store administration > Configuration > Payment
  settings > Edit > Payment methods tab (admin/store/settings/payment/edit/methods).
  Check the NBePay Payment method to enable it.

* Key in you Merchant ID and Verify Key provided by NBePay.
  *IMPORTANT* Make sure your Merchant ID and Verify Key are the same as
  in your Merchant account setting.

* By default, payment page will redirect to NBePay credit card page, where
  a side panel to let user choose the payment types (channels) is available.
  The Payment Types settings are ignored by default.

* If you prefer to let user choose the payment type within Ubercart during
  checkout, enable the "Allow customers to choose to pay by other payment
  types beside credit card." checkbox. Choose the Payment Types you would
  like to enable.


-- CREDITS --

Original author:
  Ng Chin Kiong (ckng)

Current maintainer:
  Ng Chin Kiong (ckng)

Sponsored by NetBuilder & MyFineJob.com
  Specialized in consulting and planning of Drupal powered sites, MyFineJob.com
  offers installation, development, theming, customization, and hosting
  to get you started. Visit http://www.myfinejob.com/ for more information.

